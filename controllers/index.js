const { hasKeys, cherryPickKeys } = require("../util/validation");
const errorHandler = require("../util/errorHandler");
const tokenService = require("../services/session");
const sendResponse = require("../util/response");


const addToken = async (req, res) => {
    try {
        const requiredKeys = ["userId", "ipAddress", "fingerPrint"];
        if (!hasKeys(req.body, requiredKeys))
            throw { error: "BAD_REQ", code: 400 };

        const nonRequiredKeys = [];
        const allAcceptableKeys = [...requiredKeys, ...nonRequiredKeys];
        const processedRequest = cherryPickKeys(req.body, allAcceptableKeys);

        const data = await tokenService.addToken(processedRequest);

        return sendResponse(req, res, true, 200, "TOKEN_ADDED", data);
    } catch (err) {
        const error = errorHandler(err, "Error in creating invite!");
        return sendResponse(req, res, false, error.code, error.message);
    }
};

// const verifyToken = async (req, res) => {
//     try {
//         if (!isValidObjectId(req.body.inviteId))
//             throw { error: "BAD_REQ", code: 400 };

//         const { status, inviteId } = req.body;

//         const update = { status: status || "Rejected" };
//         const condition = {
//             customer_email: req.user.email,
//             isActive: true,
//             _id: inviteId,
//         };
//         const invite = await inviteModel.updateInvite(condition, update, options);
//         if (!invite) throw { error: "NO_SUCH_INVITES", code: 400 };

//         return sendResponse(req, res, true, 200, "INVITE_UPDATED", invite);
//     } catch (err) {
//         const error = errorHandler(err, "Error in updating invite status!");
//         return sendResponse(req, res, false, error.code, error.message);
//     }
// };

const removeToken = async (req, res) => {
    try {
        const requiredKeys = ["accessToken"];
        if (!hasKeys(req.body, requiredKeys))
            throw { error: "BAD_REQ", code: 400 };

        const nonRequiredKeys = [];
        const allAcceptableKeys = [...requiredKeys, ...nonRequiredKeys];
        const processedRequest = cherryPickKeys(req.body, allAcceptableKeys);

        const data = await tokenService.removeToken(processedRequest);

        return sendResponse(req, res, true, 200, "TOKEN_REMOVED", data);
    } catch (err) {
        const error = errorHandler(err, "Error in creating invite!");
        return sendResponse(req, res, false, error.code, error.message);
    }
};

module.exports = {
    addToken,
    // verifyToken,
    removeToken
};
