require('dotenv').config();
const env = require(`./env/${process.env.ENV}.js`);
// const env = require(`./env/development.js`);

const mainRouter = require("./routes/main/index");

const mongoDB = require("./config/mongoose");
const mongoose = require("mongoose");

const express = require("express");
const app = express();
const server = require("http").createServer(app);

app.use(express.urlencoded({ extended: true, limit: "1kb" }));
app.use(express.json({ limit: "1kb" }));  // limit -  maximum request body size
 //  built-in middleware function in Express (express.urlencoded(), express.json())
// think specifically about POST requests (Not need for GET method)
// It parses incoming requests with urlencoded payloads and is based on body-parser.
// express.json() --> is a body parser for post request except html post form
// express.urlencoded({extended: false})  -->  is a body parser for html post form

mongoose.connect(mongoDB.URL, mongoDB.options); // connection with 'mongodb' database

app.use(env["API"], mainRouter);  // env["API"] -  "/session"

server.listen(env["PORT"], function () {   // env["PORT"] - 6379
    console.log("Server at port " + env["PORT"]);
});

// Insert data in collection

const sessionModel = require('./models/session');  

const collData = new sessionModel({
    accessToken: 'aaa5',
    userId: '1',
    ipAddress: '127.0.0.1',
    fingerPrint: "dkjhfrkg44895498gfng"
}); 

collData.save().then(()=>console.log('One Entry Inserted.'));

