const mongoose = require("mongoose");

const sessionSchema = new mongoose.Schema(
    {
        accessToken: {
            type: String,
            required: true,
            unique: true
        },
        userId: {
            type: String,
            required: true,
        },
        ipAddress: {
            type: String,
            required: true,
        },
        previousIPs: [
            {
                type: String,
                _id: false,
            },
        ],
        fingerPrint: {
            type: String,
            required: true,
        },
        previousFingerPrints: [
            {
                type: String,
                _id: false,
            },
        ],
        disabled: {
            type: Boolean,
            default: false,
        }
    },
    {
        timestamps: { createdAt: 'sessionStart' ,updatedAt: true },
    }
);


sessionSchema.index({ accessToken: 1, userId: 1 }, { unique: true });

sessionSchema.statics.createSession = function (session) {
    return this.create(session);
};

sessionSchema.statics.getSession = function (condition, options = {}) {
    return this.findOne(condition, options);
};

sessionSchema.statics.getSessions = function (
    condition,
    startIndex = 0,
    limit = 50,
    sort = {},
    options = {}
) {
    return this.find(condition, options)
        .limit(limit)
        .skip(startIndex)
        .sort(sort)
        .exec();
};

sessionSchema.statics.countSessions = function (condition) {
    return this.countDocuments(condition).exec();
};

sessionSchema.statics.updateSession = function (cond, update, options = {}) {
    return this.findOneAndUpdate(cond, update, options);
};

sessionSchema.statics.deleteSession = function (condition) {
    return this.findOneAndDelete(condition).exec();
};

module.exports = mongoose.model("session", sessionSchema);
