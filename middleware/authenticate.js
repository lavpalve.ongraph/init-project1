const errorHandler = require("../util/errorHandler");
const sendResponse = require("../util/response");

module.exports = async (req, res, next) => {
    try {

        next();
    } catch (err) {
        const error = errorHandler(err, "Error in processing request middleware!");
        return sendResponse(req, res, false, error["code"], error["message"]);
    }
};
