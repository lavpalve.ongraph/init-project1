module.exports = {
  system: {
    origin: "*",
    methods: "GET,PUT,PATCH,POST,DELETE",
    preflightContinue: false,
    optionsSuccessStatus: 204,
    allowedHeaders: "content-type, access-key, fingerprint, language",
    exposedHeaders: "x-auth",
  },
  service: {
    origin: "*",
    methods: "GET,POST,DELETE",
    preflightContinue: false,
    optionsSuccessStatus: 204
  },
  public: {
    origin: "*",
    methods: "GET",
    preflightContinue: false,
    optionsSuccessStatus: 204
  }
};
