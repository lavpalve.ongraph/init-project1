const sessionModel = require("../models/session");
const { createToken, saveToken, setTokenExpiry, getToken } = require("../util/token");

const addToken = async (data) => {
  try {
    data.accessToken = createToken();
    data.previousIPs = [data.ipAddress];
    data.previousFingerPrints = [data.fingerPrint];

    const session = await sessionModel.createSession(data);

    await saveToken([session.accessToken, 'userId', session.userId]);
    await setTokenExpiry(session.accessToken);

    return session;
  } catch (err) { throw err; }
};

const verifyToken = async (data) => {
  try {
    const { accessToken } = data;

    const token = await getToken(accessToken);
    if (token) return token;

    const session = await sessionModel.deleteSession({ accessToken });
    // verify and update

    return session;
  } catch (err) { throw err; }
};


const removeToken = async (data) => {
  try {
    const { accessToken } = data;

    const session = await sessionModel.deleteSession({ accessToken });

    await deleteToken(accessToken);

    return session;
  } catch (err) { throw err; }
};


module.exports = {
  addToken,
  verifyToken,
  removeToken
};
