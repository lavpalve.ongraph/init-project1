const express = require("express");
const router = express.Router();


router.get("/status", (req, res) => {
    res.status(200).send({ status: 'I am doing great here' })
});

module.exports = router;
