const express = require("express");
const router = express.Router();
// const authenticate = require("../middleware/authenticate");

const {
  addToken,
  // verifyToken,
  // removeToken
} = require("../../controllers/index.js");

router.post("/addToken", addToken);
// router.delete("/removeToken", removeToken);
// router.get("/verifyToken", authenticate, verifyToken);

module.exports = router;
