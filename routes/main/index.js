const corsOptions = require("../../config/cors");
const cors = require("cors");

const express = require("express");
const router = express.Router();

const serviceRouter = require("../service/v1");
const systemRouter = require("../system/v1");
const publicRouter = require("../public/v1");

router.use("/service", cors(corsOptions.service), serviceRouter);
router.use("/public", cors(corsOptions.public), publicRouter);
router.use("/system", cors(corsOptions.system), systemRouter);

module.exports = router;
