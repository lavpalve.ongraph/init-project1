const { v4: uuidv4 } = require('uuid');
const { createClient } = require('redis');
const client = createClient();
client.connect();

const createToken = () => uuidv4();

const saveToken = (keys) => client.hSet(...keys);

const setTokenExpiry = (key) => client.expire(key, 10);

const getToken = (key) => client.hGetAll(key);

const deleteToken = (key) => client.del(key);

module.exports = {
    createToken,
    saveToken,
    getToken,
    deleteToken,
    setTokenExpiry
};
