module.exports = (err, message = " ") => {
    if (err.code == 11000) {
        if (err.errmsg.indexOf("accessToken") > -1)
            return { message: "DUPLICATE_ACCESS_TOKEN", code: 409 };
        else
            return { message: "DUPLICATE_KEY", code: 409 };
    } else if (err.name == "ValidationError") {
        for (field in err.errors) {
            console.log(err.errors[field].message);
            return { message: err.errors[field].message, code: 400 };
        }
    } else if (err.code) {
        return { message: err.error, code: err.code };
    }
    console.log(message, err);
    return { message: "SOMETHING_WRONG", code: 500 };

};
