module.exports = (
    req,
    res,
    success,
    status,
    message = "",
    data = {},
    header = null
) => {
    if (header) {
        const head = Object.keys(header)[0];
        res.setHeader(head, header[head]);
    }
    return res.status(status).send({ success, message, data, status });
};
